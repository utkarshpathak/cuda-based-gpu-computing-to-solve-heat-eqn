#include<stdio.h>
#include<cuda_runtime.h>
#include "cublas_v2.h"
#define platesizex 1.0
#define platesizey 1.0
#define nx 200
#define ny 200
#define N nx*ny
#define hx platesizex/(nx-1)
#define hy platesizey/(ny-1)
#define g_true 1.0*(nx-1)*(nx-1)
#define f_true 1.0*(ny-1)*(ny-1)
#define d_true -2*(f_true+g_true)
#define dt 0.1
#define totaltime 1.0
#define gimp -dt*g_true // thermaldiffisivity multiplied later
#define fimp -dt*f_true // thermaldiffisivity multiplied later
#define dimp -dt*d_true // thermaldiffisivity multiplied later
#define blocksizex 16
#define blocksizey 16
#define tolerance 0.01*nx/2.0
#define sourcesizex nx/10
#define sourcesizey ny/10

__global__ void initTandToldvectors(float *t, float *told, float x0value, float y0value, float xmaxvalue, float ymaxvalue);
__global__ void initQvector(float *q, int anchorx, int anchory, float sourcetemp);
__global__ void initothervectors(float *t, float *q, float *r, float *p, float *ap, float thermaldiffusivity);
__global__ void cgkernel(float *p, float *ap, float thermaldiffusivity);
__global__ void updateP(float *p, float *r, float beta);
void cgsolver_ie();

int main()
{
 cgsolver_ie();
 return 0;
}

void cgsolver_ie()
{
 float pi = acos(-1);
 float thermaldiffusivity = 1.0;
 int anchorx = nx/2;
 int anchory = ny/2;

 // allocate arrays on GPU
 float *told = NULL;
 float *t = NULL;
 float *q = NULL;
 float *r = NULL;
 float *p = NULL;
 float *ap = NULL;
 
 // allocate memory for the arrays
 cudaMalloc(&told, N*sizeof(float));
 cudaMalloc(&t, N*sizeof(float));
 cudaMalloc(&q, N*sizeof(float));
 cudaMalloc(&r, N*sizeof(float));
 cudaMalloc(&p, N*sizeof(float));
 cudaMalloc(&ap, N*sizeof(float));
 
 // calculate block and grid size
 dim3 block(blocksizex, blocksizey);
 dim3 grid((nx+block.x-1)/block.x,(ny+block.y-1)/block.y);
 
 // initialisation of all vectors and other parameters
 initTandToldvectors<<<grid,block>>>(t,told,0,0,0,0);
 
 float r0, ptap, alpha, r1, beta, alphaneg;
 int iter;
 float time = 0;
 float timestep = dt;
 float sourcetemp = 0;
 
 // cg method begins
 cublasHandle_t handle;
 cublasStatus_t stat;
 stat = cublasCreate(&handle);
 while(time < totaltime)
 {
  initQvector<<<grid,block>>>(q,anchorx,anchory,sourcetemp);  
  stat = cublasSaxpy(handle, N, &timestep, q, 1, told, 1);
  initothervectors<<<grid,block>>>(t,told,r,p,ap,thermaldiffusivity); 
  stat = cublasSnrm2(handle, N, r, 1, &r0); 
  iter = 0; 
  while(r0 > tolerance)// && (iter < 100000))
  {
    cgkernel<<<grid,block>>>(p,ap,thermaldiffusivity);
    stat = cublasSdot(handle, N, p, 1, ap, 1, &ptap);
    alpha = (r0*r0)/ptap;
    alphaneg = -alpha;
    stat = cublasSaxpy(handle, N, &alpha, p, 1, t, 1);
    stat = cublasSaxpy(handle, N, &alphaneg, ap, 1, r, 1);
    iter++;
    stat = cublasSnrm2(handle, N, r, 1, &r1);
    if(r1 < tolerance) break;
    beta = (r1*r1)/(r0*r0);
    updateP<<<grid,block>>>(p,r,beta);
    r0 = r1;         
  }
  stat = cublasScopy(handle, N, t, 1, told, 1); 
  time = time + dt;
}
 
 cublasDestroy(handle);
 cudaFree(told);
 cudaFree(t);
 cudaFree(q);
 cudaFree(r);
 cudaFree(p);
 cudaFree(ap); 

}

__global__ 
void initTandToldvectors(float *t, float *told, float x0value, float y0value, float xmaxvalue, float ymaxvalue)
{
 int i = threadIdx.x + blocksizex * blockIdx.x;
 int j = threadIdx.y + blocksizey * blockIdx.y;     
 if(i < nx && j < ny)
 {
  int index = i+nx*j;
  float temp = 0.0;
  if(i == 0) temp = x0value;
  if(i == nx-1) temp = xmaxvalue;
  if(j == 0) temp = ymaxvalue;
  if(j == ny-1) temp = y0value; 
  t[index] = temp;
  told[index] = temp; 
 }  
}

__global__ 
void initQvector(float *q, int anchorx, int anchory, float sourcetemp)
{
 int i = threadIdx.x + blocksizex * blockIdx.x;
 int j = threadIdx.y + blocksizey * blockIdx.y;     
 if(i < nx && j < ny)
 {
  if(i > anchorx && i < anchorx+sourcesizex && j > anchory && i < anchory+sourcesizey)
  {
   q[i+nx*j] = sourcetemp;
  }
  else 
  {
   q[i+nx*j] = 0;
  }  
 }
}

__global__ 
void initothervectors(float *t, float *q, float *r, float *p, float *ap, float thermaldiffusivity)
{
 int i = threadIdx.x + blocksizex * blockIdx.x;
 int j = threadIdx.y + blocksizey * blockIdx.y;     
 if(i < nx && j < ny)
 {
  int index = i+nx*j;
  float temp = 0;
  if(i > 0 && i < nx-1 && j > 0 && j < ny-1)
  {
   temp = q[index]-thermaldiffusivity*gimp*(t[index-1]+t[index+1])-thermaldiffusivity*fimp*(t[index-nx]+t[index+nx])-(1+thermaldiffusivity*dimp)*t[index];    
  }
  r[index] = temp;
  p[index] = temp;
  ap[index] = temp;     
 }
}

__global__ 
void cgkernel(float *p, float *ap, float thermaldiffusivity)
{
 int i = threadIdx.x + blocksizex * blockIdx.x;
 int j = threadIdx.y + blocksizey * blockIdx.y;     
 if(i > 0 && i < nx-1 && j > 0 && j < ny-1)
 {
  int index = i+nx*j;
  ap[index] = thermaldiffusivity*gimp*(p[index-1]+p[index+1])+thermaldiffusivity*fimp*(p[index-nx]+p[index+nx])+(1+thermaldiffusivity*dimp)*p[index];    
 }
}

__global__ 
void updateP(float *p, float *r, float beta)
{
 int i = threadIdx.x + blocksizex * blockIdx.x;
 int j = threadIdx.y + blocksizey * blockIdx.y;     
 if(i > 0 && i < nx-1 && j > 0 && j < ny-1)
 {
  int index = i+nx*j;
  p[index] = r[index] + beta*p[index];    
 }
}

