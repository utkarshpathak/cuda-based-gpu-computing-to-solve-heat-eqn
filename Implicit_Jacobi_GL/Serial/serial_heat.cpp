#include <iostream>
#include <math.h>

#define NX 31
#define NY 31
#define dt 1.0/4096.0

float end_time [] = {1.0/8.0, 2.0/8.0, 3.0/8.0, 4.0/8.0, 5.0/8.0, 6.0/8.0, 7.0/8.0, 8.0/8.0};
int iterations[] = {8, 16, 24, 32, 40, 48, 56, 64};
float** T = new float* [NY+2];

float** initial_conditions(float** grid);
float** gauss_seidel(float** Temperature);
float** jacobi(float** Temperature);
void print_dist(float** grid);

int main() {

	//initializing the grid
	for (int i = 0; i < NY + 2; i++) {
		T[i] = new float[NX + 2];
	}

	/*initializing 2d array with initial distribution of heat
	Inside the plate it's all 1.0 on the boundries it is 0.0*/
	T = initial_conditions(T);
	std::cout << "initital Temperature Distribution" << std::endl;
	print_dist(T);
	std::cout << "*********************************" << std::endl;
	/*
	std::cout << "Distribution of heat over time calculated with Implicit time stepping scheme via Gauss Seidel algorithm" << std::endl<<std::endl;

	for (int i = 0; i < int(sizeof(end_time) / sizeof(*end_time)); i++) {
		std::cout << "Iterations: " << iterations[i] << std::endl;
		for (int j = 0; j < iterations[i]; j++) {
			T = gauss_seidel(T);
		}
		std::cout << "Heat distribution at time " << end_time[i] << std::endl;
		print_dist(T);
		T = initial_conditions(T);
	}
	*/

	std::cout << "Distribution of heat over time calculated with Explicit time stepping scheme via Jacobi algorithm" << std::endl << std::endl;

	for (int i = 0; i < int(sizeof(end_time) / sizeof(*end_time)); i++) {
		std::cout << "Iterations: " << iterations[i] << std::endl;
		for (int j = 0; j < iterations[i]; j++) {
			T = jacobi(T);
		}
		std::cout << "Heat distribution at time " << end_time[i] << std::endl;
		print_dist(T);
		T = initial_conditions(T);
	}

	return 0;
}

float** initial_conditions(float** grid) {

	/*Dirichlet boundary conditions*/
	for (int i = 0; i < NY + 2; ++i) {
		for (int j = 0; j < NX + 2; j++) {
			if ((i == 0 || i == NY + 1) || (j == 0 || j == NX + 1)) {
				grid[i][j] = 0.0;
			}
			else {
				grid[i][j] = 1.0;
			}
		}
	}
	return grid;
}

void print_dist(float** dist) {
	for (int i = 1; i < NY + 1; i++) {
		for (int j = 1; j < NX + 1; j++) {
			std::cout << dist[i][j] << " ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

float** gauss_seidel(float** Temperature) {

	/*in each iteration newely computed information will be used*/
	float hx = (1.0 / (NX + 1.0));
	float hy = (1.0 / (NY + 1.0));
	float a = dt/pow(hx, 2);
	float b = dt/pow(hy, 2);
	float c = 1.0 + 2.0 * (a) + 2.0 * (b);

	float** Temp_out = new float* [NY + 2];
	for (int i = 0; i < NY + 2; i++) {
		Temp_out[i] = new float[NX + 2];
	}

	for (int i = 0; i < NY + 2; i++) {
		for (int j = 0; j < NX + 2; j++) {
			Temp_out[i][j] = 0.0;
		}
	}

	float residual = 5.0;
	float N = NX * NY;
	float** error = new float* [NY];
	for (int i = 0; i < NY; i++) {
		error[i] = new float[NX];
		for (int j = 0; j < NX; j++) {
			error[i][j] = 0.0;
		}
	}

	int iter = 0;
	float tol = 10e-6;
	while ((tol < residual) && (iter < 10000)){    /*arbitrary number of iteration to get out
											       of the loop in unstable case*/

		//Iteration for Gauss Seidel
	    //Equation for Implicit Euler :
		//Temp_out = X + dt * Temperature
		for (int i = 1; i <= NY; i++) {
			for (int j = 1; j <= NX; j++){
				//by rearranging the implicit euler equation we get this
				Temp_out[i][j] = Temperature[i][j]/c + (a/c)*(Temp_out[i+1][j] +
					             Temp_out[i-1][j]) + (b/c)*(Temp_out[i][j+1] + Temp_out[i][j-1]);
			}
		}

		/*calculation of residual at every point of the grid
		it should be as close to zero as possible*/
		for (int i = 1; i <= NY; i++) {
			for (int j = 1; j <= NX; j++) {
				error[i - 1][j - 1] = Temperature[i][j] + a * (Temp_out[i + 1][j] + Temp_out[i - 1][j]) +
									  b * (Temp_out[i][j + 1] + Temp_out[i][j - 1]) - c * Temp_out[i][j];
			}
		}

		//residual calculation
		for (int i = 0; i < NY; i++) {
			for (int j = 0; j < NX; j++) {
				residual += pow(error[i][j], 2);
			}
		}
		residual = sqrt((1.0 / N) * residual);
		iter++;
	}

	return Temp_out;
}

float** jacobi(float** Temperature) {

	float hx = (1.0 / (NX + 1.0));
	float hy = (1.0 / (NY + 1.0));
	float a = dt / pow(hx, 2);
	float b = dt / pow(hy, 2);
	float c = 1.0 - 2.0 * (a) - 2.0 * (b);

	float** Temp_out = new float* [NY + 2];
	for (int i = 0; i < NY + 2; i++) {
		Temp_out[i] = new float[NX + 2];
	}

	for (int i = 0; i < NY + 2; i++) {
		for (int j = 0; j < NX + 2; j++) {
			Temp_out[i][j] = 0.0;
		}
	}

	/*Since it require only currently computed values to go to 
	next step all the grid points can be updated in parallel*/
	for (int i = 1; i <= NY; i++) {
		for (int j = 1; j <= NY; j++) {
			Temp_out[i][j] = c * Temperature[i][j] + a * (Temperature[i + 1][j] + Temperature[i - 1][j]) +
							 b * (Temperature[i][j + 1] + Temperature[i][j - 1]);
		}
	}

	return Temp_out;
}