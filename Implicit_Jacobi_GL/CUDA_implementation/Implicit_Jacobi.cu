#include <stdio.h>
#include <cuda_runtime.h>
#include "cublas_v2.h"
#include <math.h>

#define BLOCK_SIZE 16
#define NX 5
#define NY 5
#define N NX*NY
#define body_size_x 1.0
#define body_size_y 1.0
#define dt 1.0/64.0
typedef struct{
    float T[NX+2][NY+2];
    float T_out[NX+2][NY+2];
	float error[NX][NY];
} Grid;
Grid *h_T;
#define hx (body_size_x / (NX + 1.0))
#define hy (body_size_y / (NY + 1.0))
#define a (dt/pow(hx, 2))
#define b (dt/pow(hy, 2))
#define c (1.0 + 2.0 * (a) + 2.0 * (b))
float *r = NULL;

float end_time [] = {1.0/8.0, 2.0/8.0, 3.0/8.0, 4.0/8.0, 5.0/8.0, 6.0/8.0, 7.0/8.0, 8.0/8.0};

__global__ void grid_init(Grid *obj){
    // blockIdx, blockDim, threadIdx are built-in kernel variables.
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    
    if((i >= 0) && (i < (NX+2)) && (j >= 0) && (j < (NY+2))){   // All range
        //Dirichlet boundary conditions
        if ((i == 0 || i == NX + 1) || (j == 0 || j == NY + 1)) {
            		obj->T[i][j] = 0.0;
        }
        else {
                obj->T[i][j] = 1.0;
                
		}
    }
}

__global__ void grid_pass(Grid *obj){
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;

    if((i > 0) && (i < (NX+1)) && (j > 0) && (j < (NY+1))){
        obj->T_out[i][j] = (1/c) * obj->T[i][j] + (a/c) * (obj->T_out[i + 1][j] + obj->T_out[i - 1][j]) +
						   (b/c) * (obj->T_out[i][j + 1] + obj->T_out[i][j - 1]);
    }
}

__global__ void Tout_init(Grid *obj){
    // blockIdx, blockDim, threadIdx are built-in kernel variables.
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    
    if((i >= 0) && (i < (NX+2)) && (j >= 0) && (j < (NY+2))){   // All range
        obj->T_out[i][j] = 0.0;     
    }   
}

__global__ void error_init(Grid *obj){
    // blockIdx, blockDim, threadIdx are built-in kernel variables.
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    
    if((i >= 0) && (i < (NX)) && (j >= 0) && (j < (NY))){   // All range
        obj->error[i][j] = 0.0;   
    }     
}

__global__ void swap(Grid *obj){
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;

    if((i > 0) && (i <= (NX)) && (j > 0) && (j <= (NY))){
        obj->T[i][j] = obj->T_out[i][j];
    }
}

__global__ void error_calc(Grid *obj){
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;

    if((i > 0) && (i < (NX+1)) && (j > 0) && (j < (NY+1))){
        obj->error[i-1][j-1] = obj->T[i][j] + a * (obj->T_out[i + 1][j] + obj->T_out[i - 1][j]) +
						       b * (obj->T_out[i][j + 1] + obj->T_out[i][j - 1]) - c * obj->T_out[i][j];
    }
}

__global__ void swap_for_norm(Grid *obj, float *p){
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;

    if((i >= 0) && (i < (NX)) && (j >= 0) && (j < (NY))){
        int index = i+(NX)*j;
        p[index]= obj->error[i][j];
    }
}


void solver(){

    size_t size_matrix = sizeof(h_T->T);
    Grid *d_T;
    cublasHandle_t handle;
    cublasStatus_t stat;
    stat = cublasCreate(&handle);
    cudaMalloc(&d_T,sizeof(Grid));  
    
    dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
    
    dim3 dimGrid(ceil((NX+2)/(float)dimBlock.x), ceil((NY+2)/(float)dimBlock.y));
    
    printf("Distribution of heat over time calculated with Implicit time stepping scheme via Gauß Seidel \n");

    for (int i = 0; i < int(sizeof(end_time) / sizeof(*end_time)); i++) {
        int iterations = end_time[i]/(dt);
        printf("iter: %f, %f, %f, %i\n", dt, end_time[i], end_time[i]/(dt), iterations);
        grid_init<<<dimGrid, dimBlock>>>(d_T);
	    Tout_init<<<dimGrid, dimBlock>>>(d_T);
	    error_init<<<dimGrid, dimBlock>>>(d_T);
        for (int j = 0; j < iterations; j++) {
            int iter = 0;
            float residual = 5.0;
            float tol = 10e-6;
            float norm_ = 0.0;
            while((residual>tol) && (iter < 10000)){
                grid_pass<<<dimGrid, dimBlock>>>(d_T);
                error_calc<<<dimGrid, dimBlock>>>(d_T);
                swap_for_norm<<<dimGrid, dimBlock>>>(d_T, r);
                stat = cublasSnrm2(handle, N, r, 1, &norm_);
                residual = sqrt((1.0 / N)) * norm_;
                //cudaMemcpy(h_T->error, d_T->error, size_matrix, cudaMemcpyDeviceToHost);
                //for (int k = 0; k < NY; k++) {
			    //    for (int l = 0; l < NX; l++) {
				//        residual += pow(h_T->error[k][l], 2);
			    //    }
		        //}
                //residual = sqrt((1.0 / N) * residual);
		        iter++;
            }
            swap<<<dimGrid, dimBlock>>>(d_T);
        }
        cudaMemcpy(h_T->T, d_T->T, size_matrix, cudaMemcpyDeviceToHost);
        printf("Heat distribution at time %f \n", end_time[i]);
        for (int m=1; m<(NX+1); m++){
             for (int n=1; n<(NY+1); n++){
                 printf(" %f",h_T->T[m][n]);
             }
             printf("\n");
        }
        printf("\n");
    }

    
    cudaFree(d_T);
    printf("done \n");
}




int main(){
    h_T = (Grid*)malloc(sizeof(Grid));
    cudaMalloc(&r, N*sizeof(float));
    printf("starting Jacobi \n");
    solver();
    return 0;
}
