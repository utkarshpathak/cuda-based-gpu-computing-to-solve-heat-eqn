#include <iostream>
#include <helper_gl.h>
#include <GL/freeglut.h>
#include <string>
#include <time.h>

#include <cuda_gl_interop.h>
#include <cuda_runtime.h>
#include <helper_math.h>
#include "stdio.h"
#include "cublas_v2.h"


#define BLOCK_SIZE 16
#define NX 1024
#define NY 1024
#define N (NX*NY)
#define body_size_x 1.0
#define body_size_y 1.0
#define dt (1.0/64.0)
#define init_heat 1000.0
float *heat_source = NULL;
float init_heat_source = 1000.0;
typedef struct{
    float T[NX+2][NY+2];
    float T_out[NX+2][NY+2];
	float error[NX][NY];
} Grid;
Grid *d_T;
//Grid *h_T;
#define hx (body_size_x / (NX + 1.0))
#define hy (body_size_y / (NY + 1.0))
#define alpha (2.0)
#define a (((dt)/pow(hx, 2))*alpha)
#define b (((dt)/pow(hy, 2))*alpha)
#define c (1.0 + 2.0 * (a) + 2.0 * (b))
float *r = NULL;
double residual = 5.0;
float tol = 10e-6;
int iter = 0;
float norm_ = 0.0;

int init_time = time(NULL), final_time, fps_count = 0;

#define GL_KEY_d 100
 
#define GL_KEY_a 97
 
#define GL_KEY_s 115
 
#define GL_KEY_w 119

#define GL_KEY_plus 112

#define GL_KEY_minus 109

typedef struct{
    int start;
    int end;
}Point;

Point *x, *y;
Point *xh;
Point *yh;
uint pbo;
uint texturegl;
uchar4* dstBuffer;
uint const width = (NX);
uint const height = (NY);

    

struct cudaGraphicsResource *cuda_pbo_resource;
float *d_pbo_buffer = NULL;


__global__ void grid_init(Grid *obj){
    // blockIdx, blockDim, threadIdx are built-in kernel variables.
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    
    if((i >= 0) && (i < (NX+2)) && (j >= 0) && (j < (NY+2))){   // All range
        //Dirichlet boundary conditions
        if ((i == 0 || i == NX + 1) || (j == 0 || j == NY + 1)) {
            		obj->T[i][j] = 0.0;
        }
        else {
                obj->T[i][j] = init_heat;
                
		}
    }
}

__global__ void grid_pass(Grid *obj){
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;

    if((i > 0) && (i < (NX+1)) && (j > 0) && (j < (NY+1))){
        obj->T_out[i][j] = (1/c) * obj->T[i][j] + (a/c) * (obj->T_out[i + 1][j] + obj->T_out[i - 1][j]) +
						   (b/c) * (obj->T_out[i][j + 1] + obj->T_out[i][j - 1]);
    }
}

__global__ void Tout_init(Grid *obj){
    // blockIdx, blockDim, threadIdx are built-in kernel variables.
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    
    if((i >= 0) && (i < (NX+2)) && (j >= 0) && (j < (NY+2))){   // All range
        obj->T_out[i][j] = 0.0;     
    }   
}

__global__ void error_init(Grid *obj){
    // blockIdx, blockDim, threadIdx are built-in kernel variables.
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    
    if((i >= 0) && (i < (NX)) && (j >= 0) && (j < (NY))){   // All range
        obj->error[i][j] = 0.0;   
    }     
}

__global__ void swap(Grid *obj){
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;

    if((i > 0) && (i <= (NX)) && (j > 0) && (j <= (NY))){
        obj->T[i][j] = obj->T_out[i][j];
    }
}

__global__ void error_calc(Grid *obj){
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;

    if((i > 0) && (i < (NX+1)) && (j > 0) && (j < (NY+1))){
        obj->error[i-1][j-1] = obj->T[i][j] + a * (obj->T_out[i + 1][j] + obj->T_out[i - 1][j]) +
						       b * (obj->T_out[i][j + 1] + obj->T_out[i][j - 1]) - c * obj->T_out[i][j];
    }
}

__global__ void source(Grid *obj, Point *X, Point *Y, float *heat_source){
    
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    
    if((X->start>=0 && X->end<=NX)&&(Y->start>=0 && Y->end<=NY)&&(i >= X->start) && (i <= X->end) && (j > Y->start) && (j < Y->end)){
        obj->T[i][j] += dt*(*heat_source);
    }
}

__global__ void swap_for_norm(Grid *obj, float *p){
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;

    if((i >= 0) && (i < (NX)) && (j >= 0) && (j < (NY))){
        int index = i+(NX)*j;
        p[index]= obj->error[i][j];
    }
}

__global__ void get_heat_map(Grid *obj, uchar4* dstBuffer){
    int idx = threadIdx.x+blockDim.x*blockIdx.x;
    int idy = threadIdx.y+blockDim.y*blockIdx.y;
    if(idx >= NX || idy >= NY){return;}
    // printf("%f\n", obj->T[idx+1][idy+1]);
    dstBuffer[idy * NX + idx] = make_uchar4(255,(1 - obj->T[idx+1][idy+1])*255.0, 0, 255);
    // dstBuffer[index] = make_uchar4(255, (1- obj->T[i][j]) * 255,0, 255);
}

void solver(){
    
    dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
    dim3 dimGrid(ceil((NX+2)/(float)dimBlock.x), ceil((NY+2)/(float)dimBlock.y));
    
    cublasHandle_t handle;
    cublasStatus_t stat;
    stat = cublasCreate(&handle);
    // h_T = (Grid *)malloc(sizeof(Grid));
    // size_t size_matrix = sizeof(h_T->T);
    while((residual>tol) && (iter < 100)){
        grid_pass<<<dimGrid, dimBlock>>>(d_T);
        error_calc<<<dimGrid, dimBlock>>>(d_T);
        swap_for_norm<<<dimGrid, dimBlock>>>(d_T, r);
        stat = cublasSnrm2(handle, N, r, 1, &norm_);
        residual = sqrt((1.0 / N)) * norm_;
        iter++;
    }
    swap<<<dimGrid, dimBlock>>>(d_T);
    // cudaMemcpy(h_T->T, d_T->T, size_matrix, cudaMemcpyDeviceToHost);
    // for (int k = 0; k < NY; k++) {
    //     for (int l = 0; l < NX; l++) {
    //         printf("%f ",h_T->T[k][l]);
    //     }
    //     printf("\n");
    // }
    cudaDeviceSynchronize();
    get_heat_map<<<dimGrid, dimBlock>>>(d_T, dstBuffer);
    // printf("map acquired \n");
}


void DisplayImage( )
{
    

    cudaGraphicsMapResources(1, &cuda_pbo_resource, 0);
    size_t num_bytes;
    //get pointer to pbo and assign to destbuffer
    cudaGraphicsResourceGetMappedPointer((void**)&dstBuffer, &num_bytes, cuda_pbo_resource);

    solver();
    residual = 5.0;
    iter = 0;
    norm_ = 0.0;

    // printf("display called \n");
    //get_heat_map<<<dimGrid, dimBlock>>>(d_T, dstBuffer);
    //cudaDeviceSynchronize();
    //unmap pointer
    cudaGraphicsUnmapResources(1, &cuda_pbo_resource, 0);
    glEnable(GL_TEXTURE_2D);
    glColor3f(1.0f, 1.0f, 1.0f);
    glBindTexture(GL_TEXTURE_2D, texturegl);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, pbo);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height,  GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glPushMatrix();

    glDisable(GL_DEPTH_TEST);
    glBegin(GL_QUADS);
    glTexCoord2f(0.0f, 0.0f);
    glVertex2f(0.0f, 0.0f);
    glTexCoord2f(1.0f, 0.0f);
    glVertex2f(1.0f, 0.0f);
    glTexCoord2f(1.0f, 1.0f);
    glVertex2f(1.0f, 1.0f);
    glTexCoord2f(0.0f, 1.0f);
    glVertex2f(0.0f, 1.0f);
    glEnd();


    glPopMatrix();
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    glDisable(GL_TEXTURE_2D);
    glutSwapBuffers();
    glutPostRedisplay();

    fps_count++;
    final_time = time(NULL);
    if((final_time - init_time) > 0){
        printf("fps: %d\n", fps_count/(final_time - init_time));
        fps_count = 0;
        init_time = final_time;
    }
}

void reshapeFunc(int w, int h)
{
    glViewport(0, 0, w, h);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, 1.0, 0.0, 1.0, 0.0, 1.0);
    glutPostRedisplay();
}
void keyboard(unsigned char key, int /*x*/, int /*y*/)
{
    printf("%i\n", key);
    dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
    dim3 dimGrid(ceil((NX+2)/(float)dimBlock.x), ceil((NY+2)/(float)dimBlock.y));
    if(key>64 && key<91){
        key = key+32;
    }
    float *curr_heat_source = (float*)malloc(sizeof(float));
    switch (key)
    {
		// ESC -> kill windows
        case (27) :
            #if defined(__APPLE__) || defined(MACOSX)
                exit(EXIT_SUCCESS);
            #else
                glutDestroyWindow(glutGetWindow());
                return;
            #endif
            
        
		// Temperature - h:++, j:+, k:-, l:-- (like vim)
        case (GL_KEY_a) :
            xh->start -= 10;
            xh->end -= 10;
            
            cudaMemcpy(x, xh, sizeof(Point), cudaMemcpyHostToDevice);
            source<<<dimGrid, dimBlock>>>(d_T, x, y, heat_source);
            break;
		
		case (GL_KEY_w) :
            yh->start += 10;
            yh->end += 10;
            
            cudaMemcpy(y, yh, sizeof(Point), cudaMemcpyHostToDevice);
            source<<<dimGrid, dimBlock>>>(d_T, x, y, heat_source);
            break;
			
        case (GL_KEY_s) :
            yh->start -= 10;
            yh->end -= 10;
            
            cudaMemcpy(y, yh, sizeof(Point), cudaMemcpyHostToDevice);
            source<<<dimGrid, dimBlock>>>(d_T, x, y, heat_source);
			break;
			
        case (GL_KEY_d) :
            xh->start += 10;
            xh->end += 10;
            
            cudaMemcpy(x, xh, sizeof(Point), cudaMemcpyHostToDevice);
            source<<<dimGrid, dimBlock>>>(d_T, x, y, heat_source);
            break;
        
        case (GL_KEY_plus) :
            cudaMemcpy(curr_heat_source, heat_source, sizeof(float), cudaMemcpyDeviceToHost);
            *curr_heat_source += 100;
            cudaMemcpy(heat_source, curr_heat_source, sizeof(float), cudaMemcpyHostToDevice);
            source<<<dimGrid, dimBlock>>>(d_T, x, y, heat_source);
            break;

        case (GL_KEY_minus) :
            cudaMemcpy(curr_heat_source, heat_source, sizeof(float), cudaMemcpyDeviceToHost);
            *curr_heat_source -= 100;
            if(*curr_heat_source < 0){*curr_heat_source = 0;}
            cudaMemcpy(heat_source, curr_heat_source, sizeof(float), cudaMemcpyHostToDevice);
            source<<<dimGrid, dimBlock>>>(d_T, x, y, heat_source);
            break;
			
    }
}


int main(int argc, char** argv){

    /*Launching kernel to initialize grid*/
    cudaMalloc(&d_T, sizeof(Grid));
    cudaMalloc(&x, sizeof(Point));
    cudaMalloc(&y, sizeof(Point));
    cudaMalloc(&r, N*sizeof(float));
    cudaMalloc(&heat_source, sizeof(float));
    xh = (Point*)malloc(sizeof(Point));
    yh = (Point*)malloc(sizeof(Point));
    xh->start = (NX/2) - 50;
    xh->end = (NX/2) + 50;
    yh->start = (NY/2) - 50;
    yh->end = (NY/2) + 50;
    cudaMemcpy(x, xh, sizeof(Point), cudaMemcpyHostToDevice);
    cudaMemcpy(y, yh, sizeof(Point), cudaMemcpyHostToDevice);
    cudaMemcpy(heat_source, &init_heat_source, sizeof(float), cudaMemcpyHostToDevice);
    dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
    dim3 dimGrid(ceil((NX+2)/(float)dimBlock.x), ceil((NY+2)/(float)dimBlock.y));
    
    grid_init<<<dimGrid, dimBlock>>>(d_T);
	Tout_init<<<dimGrid, dimBlock>>>(d_T);
	error_init<<<dimGrid, dimBlock>>>(d_T);
    
    //init device memory
    uint const size = width* height ;
    uchar4 *h_dst = (uchar4 *)malloc(size*sizeof(uchar4)); 
    cudaMalloc(&dstBuffer, size*sizeof(uchar4));
    //dim3 dimGrid2(ceil((NX)/(float)dimBlock.x), ceil((NY)/(float)dimBlock.y));
    //cudaMemcpy(h_dst, dstBuffer, size*sizeof(uchar4), cudaMemcpyDeviceToHost);
    
    //init opengl
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowSize(NX,NY);
    glutCreateWindow("Distribution of heat over time.");
    glutDisplayFunc(DisplayImage);
    glutReshapeFunc(reshapeFunc);
    glutKeyboardFunc(keyboard);

    glEnable(GL_TEXTURE_2D);
    //generate an opengl texture
    glGenTextures( 1, &texturegl );
    glBindTexture( GL_TEXTURE_2D, texturegl );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    // Create texture data (4-component unsigned byte)
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL );

// generate pbo
    glGenBuffers(1, &pbo);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, pbo);
    glBufferData(GL_PIXEL_UNPACK_BUFFER_ARB, width * height * sizeof(uchar4),
    0, GL_STREAM_COPY);
//register pbo to cuda resource
    cudaGraphicsGLRegisterBuffer(&cuda_pbo_resource, pbo, cudaGraphicsMapFlagsWriteDiscard);

//start main gopengl loop
    glutMainLoop();

    cudaFree(dstBuffer);
    cudaFree(d_T);
    return 0;
}

