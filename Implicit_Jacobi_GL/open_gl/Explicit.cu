#include <stdio.h>
#include <cuda_runtime.h>
//#include "cublas_v2.h"
#include <math.h>

#define BLOCK_SIZE 16
#define NX 5
#define NY 5
#define N NX*NY
#define body_size_x 1.0
#define body_size_y 1.0
#define residual 5.0
#define dt 1.0/256.0
typedef struct{
    float T[NX+2][NY+2];
    float T_out[NX+2][NY+2];
} Grid;
Grid *h_T;
#define hx (body_size_x / (NX + 1.0))
#define hy (body_size_y / (NY + 1.0))
#define a (dt/pow(hx, 2))
#define b (dt/pow(hy, 2))
#define c (1.0 - 2.0 * (a) - 2.0 * (b))
float end_time [] = {1.0/8.0, 2.0/8.0, 3.0/8.0, 4.0/8.0, 5.0/8.0, 6.0/8.0, 7.0/8.0, 8.0/8.0};
//int iterations[] = {8, 16, 24, 32, 40, 48, 56, 64};


__global__ void grid_init(Grid *obj){
    // blockIdx, blockDim, threadIdx are built-in kernel variables.
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    
    if((i >= 0) && (i < (NX+2)) && (j >= 0) && (j < (NY+2))){   // All range
        //Dirichlet boundary conditions
        if ((i == 0 || i == NX + 1) || (j == 0 || j == NY + 1)) {
            		obj->T[i][j] = 0.0;
        }
        else {
                obj->T[i][j] = 1.0;
                
		}
    }
}

__global__ void grid_pass(Grid *obj){
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;

    if((i > 0) && (i < (NX+1)) && (j > 0) && (j < (NY+1))){
        obj->T_out[i][j] = c * obj->T[i][j] + a * (obj->T[i + 1][j] + obj->T[i - 1][j]) +
						 b * (obj->T[i][j + 1] + obj->T[i][j - 1]);
    }
}

__global__ void swap(Grid *obj){
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;

    if((i > 0) && (i <= (NX)) && (j > 0) && (j <= (NY))){
        obj->T[i][j] = obj->T_out[i][j];
    }
}

void jacobi(){

    size_t size_matrix = sizeof(h_T->T);
    Grid *d_T;
    cudaMalloc(&d_T,sizeof(Grid));  
    // printf("block init \n");
    dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
    // printf("grid inti \n");
    dim3 dimGrid(ceil((NX+2)/(float)dimBlock.x), ceil((NY+2)/(float)dimBlock.y));
    // printf("init via boundary %f, %f\n", ceil((NX+2)/(float)dimBlock.x), ceil((NY+2)/(float)dimBlock.y));
    grid_init<<<dimGrid, dimBlock>>>(d_T);
    // printf("initital Temperature Distribution\n");
    // cudaMemcpy(h_T->T, d_T->T, size_matrix, cudaMemcpyDeviceToHost);
    // for (int i=1; i<(NX+1); i++){
    //     for (int j=1; j<(NY+1); j++){
    //         printf(" %f",h_T->T[i][j]);
    //     }
    //     printf("\n");
    // }
    cudaMemcpy(h_T->T, d_T->T, size_matrix, cudaMemcpyDeviceToHost);
    printf("Distribution of heat over time calculated with Explicit time stepping scheme via Jacobi algorithm \n");

    for (int i = 0; i < int(sizeof(end_time) / sizeof(*end_time)); i++) {
        int iterations = end_time[i]/(dt);
        printf("iter: %f, %f, %f, %i\n", dt, end_time[i], end_time[i]/(dt), iterations);
        for (int j = 0; j < iterations; j++) {
            grid_pass<<<dimGrid, dimBlock>>>(d_T);
            swap<<<dimGrid, dimBlock>>>(d_T);
        }
        cudaMemcpy(h_T->T, d_T->T, size_matrix, cudaMemcpyDeviceToHost);
        // printf("Heat distribution at time %f \n", end_time[i]);
        // for (int i=1; i<(NX+1); i++){
        //     for (int j=1; j<(NY+1); j++){
        //         printf(" %f",h_T->T[i][j]);
        //     }
        //     printf("\n");
        // }
        // printf("\n");
        grid_init<<<dimGrid, dimBlock>>>(d_T);
    }

    
    cudaFree(d_T);
    printf("done \n");
}




int main(){
    h_T = (Grid*)malloc(sizeof(Grid));
    printf("starting Jacobi \n");
    jacobi();
    return 0;
}
