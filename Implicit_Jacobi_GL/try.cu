#include <stdio.h>
#include <cuda_runtime.h>
#include "cublas_v2.h"
#define NX 5
#define NY 5
#define BLOCK_SIZE 16
typedef struct{
    float T[NX+2][NY+2];
} Grid;
Grid *h_T;
float res = 0;
float *r = NULL;

__global__ void grid_init(Grid *obj){
    // blockIdx, blockDim, threadIdx are built-in kernel variables.
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    
    if((i >= 0) && (i < (NX+2)) && (j >= 0) && (j < (NY+2))){   // All range
        //Dirichlet boundary conditions
        if ((i == 0 || i == NX + 1) || (j == 0 || j == NY + 1)) {
            		obj->T[i][j] = 0.0;
        }
        else {
                obj->T[i][j] = 1.0;
                
		}
    }
}

__global__ void swap(Grid *obj, float *r){
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;

    if((i > 0) && (i <= (NX)) && (j > 0) && (j <= (NY))){
        r[(i+NX*j)] = obj->T[i][j];
    }
}


void _norm_(){
	//size_t size_matrix = sizeof(h_T->T);
    Grid *d_T;
    cudaMalloc(&d_T,sizeof(Grid)); 
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
    
    dim3 dimGrid(ceil((NX+2)/(float)dimBlock.x), ceil((NY+2)/(float)dimBlock.y));
	
	cudaMalloc(&r, (NX+2)*(NY+2)*sizeof(float));
	grid_init<<<dimGrid, dimBlock>>>(d_T);
	swap<<<dimGrid, dimBlock>>>(d_T, r);
	cublasHandle_t handle;
 	cublasStatus_t stat;
	stat = cublasCreate(&handle);
	
	stat = cublasSnrm2(handle, ((NX)*(NY)), r, 1, &res); 
	printf("%f: \n", res);
	
}

int main(){
	h_T = (Grid*)malloc(sizeof(Grid));
	_norm_();
	return 0;
}
