#include "linear_algebra.h"

void scaleMat(double c, matrix A)
{
 unsigned int m = A.m;
 unsigned int n = A.n;
 double** a = A.a; 

 int i;
 int j;
 for (i = 0; i < m; i++)
 for (j = 0; j < n; j++)
 {
  a[i][j] = c * a[i][j];	 
 }  
}


