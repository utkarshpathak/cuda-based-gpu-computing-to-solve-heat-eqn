#include "linear_algebra.h"

matrix initOnesMat(unsigned int m, unsigned int n)
{
 matrix A;  
 A.m = m;
 A.n = n;
 A.a = malloc(sizeof(double*)*m);
 double** a = A.a;
 int init_rows;
 for (init_rows = 0; init_rows < m; init_rows++)
 {
  a[init_rows] = malloc(sizeof(double)*n);
 }
 
 int i;
 int j;
 for (i = 0; i < m; i++)
 for (j = 0; j < n; j++)
 {
  a[i][j] = 1;	 
 }   
 return A; 
}


