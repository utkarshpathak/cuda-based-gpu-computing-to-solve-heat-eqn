#ifndef LINEAR_ALGEBRA_H_INCLUDED
#define LINEAR_ALGEBRA_H_INCLUDED
 
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include<unistd.h>
#include<omp.h>
#include "linear_algebra_structures.h"
#include "linear_algebra_functions.h"
 
#endif
