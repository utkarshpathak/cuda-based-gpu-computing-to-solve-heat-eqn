#include "linear_algebra.h"

void addVec(vector c, vector a, vector b)
{  
 if((a.dimension != b.dimension) || (a.dimension != c.dimension))
 {
  printf("Vector dimensions not equal. Cannot add. Exiting program. \n");
  exit (1);
 }	

 double* va = a.u;
 double* vb = b.u;
 double* vc = c.u;
 unsigned int dim = c.dimension; 
 int i;
 for (i = 0; i < dim; i++)
 {
  vc[i] = va[i] + vb[i];
 } 
}


