#include "linear_algebra.h"

double infNormVec(vector a)
{  
 double norm = 0; 
 double* va = a.u;
 unsigned int dim = a.dimension; 
 int i;
 for (i = 0; i < dim; i++)
 {
   if(fabs(va[i]) > norm) norm = fabs(va[i]);  
 } 
 return norm;
}


