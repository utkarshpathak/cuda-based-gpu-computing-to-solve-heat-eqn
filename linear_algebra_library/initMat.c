#include "linear_algebra.h"

matrix initMat(matrix A) 
{
 matrix B;  
 B.m = A.m;
 B.n = A.n;

 unsigned int m = B.m;
 unsigned int n = B.n; 
 B.a = malloc(sizeof(double*)*m);
 double** a = B.a;
 int init_rows;
 for (init_rows = 0; init_rows < m; init_rows++)
 {
  a[init_rows] = malloc(sizeof(double)*n);
 }	  
 return B;
}


