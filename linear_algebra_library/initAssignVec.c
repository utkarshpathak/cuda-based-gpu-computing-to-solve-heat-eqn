#include "linear_algebra.h"

vector initAssignVec(vector b)
{
 vector a;
 a.dimension = b.dimension;
 a.u = malloc(sizeof(double)*a.dimension); 
 double* va = a.u;
 double* vb = b.u;
 unsigned int dim = a.dimension; 
 int i;
 for (i = 0; i < dim; i++)
 {
  va[i] = vb[i];
 }
 return a;
}

