#include "linear_algebra.h"

void assignMat(matrix A, matrix B)
{

 if((A.m != B.m) || (A.n != B.n))
 {
  printf("Matrix dimensions not equal. Cannot assign the matrices. Exiting program. \n");
  exit (1);
 }

 unsigned int m = A.m;
 unsigned int n = A.n;
 double** a = A.a; 
 double** b = B.a;
 
 int i;
 int j;
 for (i = 0; i < m; i++)
 for (j = 0; j < n; j++)
 {
  a[i][j] = b[i][j];	 
 }  
}


