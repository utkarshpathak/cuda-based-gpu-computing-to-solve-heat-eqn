#include"linear_algebra.h"
#define TYPE double 
#define THREADS 1

matrix generateTMatrix(int nx, int ny);
matrix generateSourceMatrix(int nx, int ny);
void correctBoundaryValues(matrix A, TYPE x0value, TYPE y0value, TYPE xmaxvalue, TYPE ymaxvalue);
void calculateSourceMatrix(matrix R);
void matrixFreeJocobiSolver(matrix T, matrix R, TYPE tolerance);
void matrixFreeSORSolver(matrix T, matrix R, TYPE w, TYPE tolerance);
void matrixFreeCGSolver(matrix X, matrix B, TYPE tolerance);
matrix generateAnalyticalSolution(int nx, int ny);
void calculateAnalyticalSolution(matrix S);

int main()
{
  int nx = 10000, ny = 10000;
  matrix T = generateTMatrix(ny, nx);
  matrix R = generateSourceMatrix(ny, nx);
  //matrixFreeJocobiSolver(T, R, 0.001);
  //matrixFreeSORSolver(T, R, 1.2, 0.001);
  matrixFreeCGSolver(T, R, 0.001);
  matrix S = generateAnalyticalSolution(ny, nx);
  subMat(S, S, T);
  printf("Error norm is %f\n",fNorm(S)); 
  delMat(S);
  delMat(R);
  delMat(T);	  	 
  return 0;
}

matrix generateTMatrix(int nx, int ny) 
{
 matrix T = initNullMat(nx, ny); // initOnesMat in the case of dynamic heat eqn.
 correctBoundaryValues(T, 0, 0, 0, 0); 
 return T;
}

void correctBoundaryValues(matrix A, TYPE x0value, TYPE y0value, TYPE xmaxvalue, TYPE ymaxvalue)
{
 int m = A.m;
 int n = A.n;
 TYPE **a = A.a;
 int i;
 for(i = 0; i < n; i++) a[0][i] = ymaxvalue;
 for(i = 0; i < n; i++) a[m-1][i] = y0value;
 for(i = 1; i < m-1; i++) a[i][0] = x0value;
 for(i = 1; i < m-1; i++) a[i][n-1] = xmaxvalue;
} 

matrix generateSourceMatrix(int nx, int ny)
{
 matrix R = initNullMat(nx, ny);
 calculateSourceMatrix(R); 
 return R;
}

void calculateSourceMatrix(matrix R)
{
 TYPE pi = acos(-1);
 int m = R.m;
 int n = R.n;
 TYPE hx = 1.0/(n-1); // for x dimension
 TYPE hy = 1.0/(m-1); // for y dimension    
 TYPE** r = R.a;
 int i;
 int j;
 for(i = 0; i < m; i++)
 for(j = 0; j < n; j++)
 r[i][j] = -2*pi*pi*sin(pi*j*hx)*sin(pi*(m-1-i)*hy); // x=j*hx,y=(m-1-i)*hy 
}

void matrixFreeJocobiSolver(matrix T, matrix R, TYPE tolerance)
{ 
 register int m = T.m;
 register int n = T.n;
 TYPE** t = T.a;
 register TYPE g = (n-1)*(n-1); // for x dimension
 register TYPE f = (m-1)*(m-1); // for y dimension 
 register TYPE d = -2*(f+g); 
 
 TYPE** r = R.a;
 
 matrix Tn = initAssignMat(T);
 TYPE** tn = Tn.a;
 
 register TYPE residual = 100; 
 
 register int i, j, iter = 0;
 register TYPE temp;
 TYPE** tmp = NULL;
 tolerance = tolerance * tolerance;
 while((residual > tolerance))// && (iter < 100000))
 {
  #pragma omp parallel for num_threads(THREADS)
  for(i = 1; i < m-1; i++)
  for(j = 1; j < n-1; j++)
  {
   t[i][j] = (r[i][j]-g*(tn[i][j-1]+tn[i][j+1])-f*(tn[i-1][j]+tn[i+1][j]))/d;
  }
  iter++;
    
  tmp = tn;
  tn = t;
  t = tmp;
    
  //if(iter%1000 == 0)
  {
   residual = 0;
   for(i = 1; i < m-1; i++)
   for(j = 1; j < n-1; j++)
   {
    temp = r[i][j]-g*(tn[i][j-1]+tn[i][j+1])-f*(tn[i-1][j]+tn[i+1][j])-d*tn[i][j];
    residual = residual+temp*temp;   
   }
   //printf("%f\n", sqrt(residual));
  }
 }
 //printf("Jacobi iteration:%i, Residual:%f\n", iter, sqrt(residual));
}

void matrixFreeSORSolver(matrix T, matrix R, TYPE w, TYPE tolerance)
{ 
 register int m = T.m;
 register int n = T.n;
 TYPE** t = T.a;
 register TYPE g = (n-1)*(n-1); // for x dimension
 register TYPE f = (m-1)*(m-1); // for y dimension 
 register TYPE d = -2*(f+g); 
 
 TYPE** r = R.a;
 
 matrix Tn = initAssignMat(T);
 TYPE** tn = Tn.a;
 
 register TYPE residual = 100; 
 
 register int i, j, iter = 0;
 register TYPE temp, sum, omega = w;

 tolerance = tolerance * tolerance;
 while((residual > tolerance))// && (iter < 100000))
 {
  for(i = 1; i < m-1; i++)
  for(j = 1; j < n-1; j++)
  {
   t[i][j] = (r[i][j]-g*(t[i][j-1]+t[i][j+1])-f*(t[i-1][j]+t[i+1][j]))/d;
  }
  iter++;
  
  scaleMat((1-omega), Tn); // (1-w)*Tn 
  scaleMat(omega, T); // w*T
  addMat(T, T, Tn); // T = w*T + (1-w)*Tn 
  assignMat(Tn, T);
      
  if(iter%100 == 0)
  {
   residual = 0;
   for(i = 1; i < m-1; i++)
   for(j = 1; j < n-1; j++)
   {
    temp = r[i][j]-g*(t[i][j-1]+t[i][j+1])-f*(t[i-1][j]+t[i+1][j])-d*t[i][j];
    residual = residual+temp*temp;   
   }
   //printf("SOR iteration (w=%f):%i, Residual:%f\n", omega, iter, sqrt(residual));
  }
 }
}

void matrixFreeCGSolver(matrix X, matrix B, TYPE tolerance)
{ 
 scaleMat(-1, B);	
 register int m = X.m;
 register int n = X.n;
 TYPE** x = X.a;
 register TYPE g = (n-1)*(n-1); // for x dimension
 register TYPE f = (m-1)*(m-1); // for y dimension 
 register TYPE d = -2*(f+g);  
 g = -1*g;
 f = -1*f;
 d = -1*d;
 
 TYPE** b = B.a;
 
 matrix R = initAssignMat(X);
 TYPE** r = R.a;
 
 register int i, j;
 #pragma omp parallel for num_threads(THREADS)
 for(i = 1; i < m-1; i++)
 for(j = 1; j < n-1; j++)
 {
  r[i][j] = b[i][j]-g*(x[i][j-1]+x[i][j+1])-f*(x[i-1][j]+x[i+1][j])-d*x[i][j];
 }
 
 TYPE r0 = fNorm(R);
 if(r0 < tolerance) return;
 
 matrix P = initAssignMat(R);
 TYPE** p = P.a;
 
 matrix AP = initAssignMat(R);
 TYPE** ap = AP.a;
 
 TYPE ptap;
 TYPE alpha;
 TYPE r1;
 TYPE beta;
 
 int iter = 0;
 while((r0 > tolerance))// && (iter < 100000))
 {
  #pragma omp parallel for num_threads(THREADS)
  for(i = 1; i < m-1; i++)
  for(j = 1; j < n-1; j++)
  {
   ap[i][j] = g*(p[i][j-1]+p[i][j+1])+f*(p[i-1][j]+p[i+1][j])+d*p[i][j];
  }
  ptap = hadamardProductNorm(P, AP);
  alpha = (r0*r0)/(ptap*ptap);
  
  scaleMat(alpha, P);
  addMat(X, X, P);
  iter++;
    
  scaleMat(alpha, AP);
  subMat(R, R, AP);
  
  r1 = fNorm(R);
  if(r1 < tolerance) break;
  
  beta = (r1*r1)/(r0*r0);
  
  scaleMat(beta/alpha, P);
  addMat(P, R, P);
  
  r0 = r1;    
  printf("CG Solver iterations:%i, Residual:%f\n", iter, r1); 
 } 
 
 printf("CG Solver iterations:%i, Residual:%f\n", iter, r1);
}

matrix generateAnalyticalSolution(int nx, int ny)
{
 matrix S = initNullMat(nx, ny);
 calculateAnalyticalSolution(S); 
 return S;
}

void calculateAnalyticalSolution(matrix S)
{
 TYPE pi = acos(-1);
 int m = S.m;
 int n = S.n;
 TYPE hx = 1.0/(n-1); // for x dimension
 TYPE hy = 1.0/(m-1); // for y dimension    
 TYPE** s = S.a;
 int i;
 int j;
 for(i = 0; i < m; i++)
 for(j = 0; j < n; j++)
 s[i][j] = sin(pi*j*hx)*sin(pi*(m-1-i)*hy); // x=j*hx,y=(m-1-i)*hy 
}

