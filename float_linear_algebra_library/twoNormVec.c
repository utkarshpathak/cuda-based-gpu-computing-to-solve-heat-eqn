#include "linear_algebra.h"

float twoNormVec(vector a)
{  
 float norm = 0; 
 float* va = a.u;
 unsigned int dim = a.dimension; 
 int i;
 for (i = 0; i < dim; i++)
 {
  norm = norm + va[i] * va[i];
 } 
 return sqrt(norm);
}


