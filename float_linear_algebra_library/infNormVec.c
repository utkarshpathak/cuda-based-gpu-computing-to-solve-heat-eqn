#include "linear_algebra.h"

float infNormVec(vector a)
{  
 float norm = 0; 
 float* va = a.u;
 unsigned int dim = a.dimension; 
 int i;
 for (i = 0; i < dim; i++)
 {
   if(fabs(va[i]) > norm) norm = fabs(va[i]);  
 } 
 return norm;
}


