#include "linear_algebra.h"

void transposeMat(matrix B, matrix A)
{

 if((A.m != B.n) || (A.n != B.m))
 {
  printf("Matrix dimensions not compatible. Cannot transpose the matrices. Exiting program. \n");
  exit (1);
 }

 unsigned int m = B.m;
 unsigned int n = B.n;
 float** a = A.a; 
 float** b = B.a;
 
 int i;
 int j;
 for (i = 0; i < m; i++)
 for (j = 0; j < n; j++)
 {
  b[i][j] = a[j][i];	 
 }  
}


