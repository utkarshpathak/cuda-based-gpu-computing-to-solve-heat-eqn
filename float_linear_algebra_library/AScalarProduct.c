#include "linear_algebra.h"

float AScalarProduct(vector u, matrix A, vector v)
{

 if((A.n != v.dimension) || (A.m != u.dimension))
 {
  printf("Matrix vector dimensions not matching for scalar product. Exiting program. \n");
  exit (1);
 }

 unsigned int m = A.m;
 unsigned int n = A.n;
 
 float** a = A.a; 
 float* va = u.u;
 float* vb = v.u;
 
 float scalar_product = 0;
 int i;
 int j;
 for (i = 0; i < m; i++)
 {
  float sum = 0;
  for (j = 0; j < n; j++)
  {
   sum = sum + a[i][j] * vb[j];
  }
  scalar_product = scalar_product + va[i] * sum;	 
 }  

 return scalar_product;
}


