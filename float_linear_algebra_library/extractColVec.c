#include "linear_algebra.h"

void extractColVec(matrix A, unsigned int c, vector u)
{

 if(A.m != u.dimension)
 {
  printf("Matrix vector dimensions not matching for extracting. Exiting program. \n");
  exit (1);
 }

 unsigned int m = A.m;
 
 float** a = A.a; 
 float* x = u.u;
 int i;
 for (i = 0; i < m; i++)
 {
  x[i] = a[i][c-1];
 }
}


