#include "linear_algebra.h"

void scaleVec(float c, vector a)
{  
 float* va = a.u;
 unsigned int dim = a.dimension; 
 int i;
 for (i = 0; i < dim; i++)
 {
  va[i] = c * va[i];
 } 
}


