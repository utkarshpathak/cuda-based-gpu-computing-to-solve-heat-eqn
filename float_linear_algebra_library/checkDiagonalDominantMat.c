#include "linear_algebra.h"

unsigned int checkDiagonalDominantMat(matrix A)
{

 if(A.m != A.n)
 {
  printf("Matrix dimensions are not equal. Matrix is not square. Exiting program. \n");
  exit (1);
 }

 unsigned int m = A.m;
 float** a = A.a; 
 
 unsigned int is_diagonally_dominant = 1;
 float row_sum;
 int i;
 int j;
 for (i = 0; i < m; i++)
 {
  row_sum = 0; 
  for (j = 0; j < m; j++)
  {
   row_sum = row_sum + fabs(a[i][j]); 	 
  }
  row_sum = row_sum - fabs(a[i][i]);
  if(row_sum > fabs(a[i][i])) 
  {
   is_diagonally_dominant = 0;
   break; 
  } 
 }
 return is_diagonally_dominant;  
}


