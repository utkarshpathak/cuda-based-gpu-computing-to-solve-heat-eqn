#include "linear_algebra.h"

unsigned int checkEqualityMat(matrix A, matrix B)
{

 if((A.m != B.m) || (A.n != B.n))
 {
  printf("Matrix dimensions not equal. Cannot check the equality of matrices. Exiting program. \n");
  exit (1);
 }

 unsigned int m = A.m;
 unsigned int n = A.n;
 float** a = A.a; 
 float** b = B.a;
 
 unsigned int are_equal = 1;
 int i;
 int j;
 for (i = 0; i < m; i++)
 for (j = 0; j < n; j++)
 {
  if(a[i][j] != b[i][j]) 
  {
   are_equal = 0;
   i = m;
   break; 
  }  	 
 }
 return are_equal;  
}


