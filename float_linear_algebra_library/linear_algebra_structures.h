#ifndef LINEAR_ALGEBRA_STRUCTURES_H_INCLUDED
#define LINEAR_ALGEBRA_STRUCTURES_H_INCLUDED

typedef struct
{
 unsigned int dimension;
 float* u; 
} vector;

typedef struct
{
 unsigned int m; // no. of rows
 unsigned int n; // no. of columns
 float** a; 
} matrix;

#endif
