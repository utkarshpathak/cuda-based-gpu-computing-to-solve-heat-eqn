#include "linear_algebra.h"

float scalarProdVec(vector a, vector b)
{  
 if((a.dimension != b.dimension))
 {
  printf("Vector dimensions not equal. Cannot assign the vectors. Exiting program. \n");
  exit (1);
 }	
 
 float scalar_product = 0; 
 float* va = a.u;
 float* vb = b.u;
 unsigned int dim = a.dimension; 
 int i;
 for (i = 0; i < dim; i++)
 {
  scalar_product = scalar_product + va[i] * vb[i];
 } 
 return scalar_product;
}


