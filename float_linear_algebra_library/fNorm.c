#include "linear_algebra.h"

float fNorm(matrix A)
{
 unsigned int m = A.m;
 unsigned int n = A.n;
 float** a = A.a; 

 float norm = 0;
 int i;
 int j;
 for (i = 0; i < m; i++)
 for (j = 0; j < n; j++)
 {
  norm = norm + a[i][j] * a[i][j];	 
 }
 return sqrt(norm);  
}


