This library performs linear algebra operations. To create the library, follow the steps:

1. compile the optimized object files with the link-time optimization option: gcc -c -O3 -flto *.c

2. create the library using ar utility: ar -cvq liblinearalgebra.a *.o

Object files present in the library can be checked using: ar -t liblinearalgebra.a


 
