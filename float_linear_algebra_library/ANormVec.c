#include "linear_algebra.h"

float ANormVec(vector u, matrix A)
{

 if((A.n != u.dimension) || (A.m != u.dimension))
 {
  printf("Matrix vector dimensions not matching for calculating norm. Exiting program. \n");
  exit (1);
 }

 return sqrt(AScalarProduct(u, A, u));
}


