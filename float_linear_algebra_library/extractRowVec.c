#include "linear_algebra.h"

void extractRowVec(matrix A, unsigned int r, vector u)
{

 if(A.n != u.dimension)
 {
  printf("Matrix vector dimensions not matching for extracting. Exiting program. \n");
  exit (1);
 }

 unsigned int n = A.n;
 
 float** a = A.a; 
 float* x = u.u;
 int j;
 for (j = 0; j < n; j++)
 {
  x[j] = a[r-1][j];
 }
}


