#include "linear_algebra.h"

void printVec(vector a)
{
 printf("\n");
 float* va = a.u;
 unsigned int dim = a.dimension;
 int i;
 for (i = 0; i < dim; i++)
 {
  printf("%f\n", va[i]);
 } 
 printf("\n");
}


