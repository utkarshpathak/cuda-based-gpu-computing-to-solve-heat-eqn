#include "linear_algebra.h"

void subVec(vector c, vector a, vector b)
{  
 if((a.dimension != b.dimension) || (a.dimension != c.dimension))
 {
  printf("Vector dimensions not equal. Cannot subtract. Exiting program. \n");
  exit (1);
 }	

 float* va = a.u;
 float* vb = b.u;
 float* vc = c.u;
 unsigned int dim = c.dimension;
 int i;
 for (i = 0; i < dim; i++)
 {
  vc[i] = va[i] - vb[i];
 } 
}


