#ifndef LINEAR_ALGEBRA_FUNCTIONS_H_INCLUDED
#define LINEAR_ALGEBRA_FUNCTIONS_H_INCLUDED

#define CONST_K 1




// PURE VECTOR FUNCTIONS

vector initNullVec(unsigned int dim);

vector initVec(vector b);

vector initAssignVec(vector b);

void assignVec(vector a, vector b);

void addVec(vector c, vector a, vector b);

void subVec(vector c, vector a, vector b);

void scaleVec(float c, vector a);

unsigned int getDimVec(vector a);

float oneNormVec(vector a);

float twoNormVec(vector a);

float infNormVec(vector a);

void printVec(vector a);

void delVec(vector a);

float scalarProdVec(vector a, vector b);

// vector product of vectors





// PURE MATRIX FUNCTIONS

matrix initNullMat(unsigned int m, unsigned int n); 

matrix initOnesMat(unsigned int m, unsigned int n);

matrix initIMat(unsigned int dimension);

matrix initMat(matrix A); 

matrix initAssignMat(matrix A); 

void assignMat(matrix A, matrix B);

void addMat(matrix C, matrix A, matrix B);

void subMat(matrix C, matrix A, matrix B);

void scaleMat(float c, matrix A);

void multMat(matrix C, matrix A, matrix B); // do it so that if B = C then temp_matrix = A*B and then B = temp_matrix

unsigned int getRowMat(matrix A);

unsigned int getColMat(matrix A);

void printMat(matrix A);

void delMat(matrix A);

unsigned int checkEqualityMat(matrix A, matrix B);

unsigned int checkSymmetricMat(matrix A);

unsigned int checkDiagonalDominantMat(matrix A);

void transposeMat(matrix B, matrix A); // Do it so that if A = B then A gets transposed

float fNorm(matrix A);

float hadamardProductNorm(matrix A, matrix B);

// 1 norm, infinity norm, 2 norm using power iteration

// QR decomposition

// LU decomposition

// extract E, D, D_inverse, F, L, U matrices from a matrix

// extract any sub-matrix from the matrix bwtween rows m1 to m2 and columns n1 to n2

// check if matrix dimensions are equal

// QR algorithm for eigenpair decomposition

// can write a generalised function that writes in diagonals of a given matrix a fixed value

// mutliply a matrix with a diagonal matrix

// inverse a diagonal matrix 




// MATRIX VECTOR FUNCTIONS

void multMatVec(vector v, matrix A, vector u);

void extractRowVec(matrix A, unsigned int r, vector u);

void extractColVec(matrix A, unsigned int c, vector u);

float AScalarProduct(vector u, matrix A, vector v);

float ANormVec(vector u, matrix A);

// mutliply vector with a diagonal matrix

// Jacobi relaxation

// SOR relaxation

// Direct solver

// CG

// PCG




#endif
