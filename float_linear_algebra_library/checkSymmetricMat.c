#include "linear_algebra.h"

unsigned int checkSymmetricMat(matrix A)
{

 if(A.m != A.n)
 {
  printf("Matrix dimensions are not equal. Matrix is not square. Exiting program. \n");
  exit (1);
 }

 unsigned int m = A.m;
 float** a = A.a; 
 
 unsigned int is_symmetric = 1;
 int i;
 int j;
 for (i = 0; i < m; i++)
 for (j = i+1; j < m; j++)
 {
  if(a[i][j] != a[j][i]) 
  {
   is_symmetric = 0;
   i = m;
   break; 
  }  	 
 }
 return is_symmetric;  
}


