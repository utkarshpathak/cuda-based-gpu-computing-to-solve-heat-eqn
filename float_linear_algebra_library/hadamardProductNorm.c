#include "linear_algebra.h"

float hadamardProductNorm(matrix A, matrix B)
{
 unsigned int m = A.m;
 unsigned int n = A.n;
 float** a = A.a;
 float** b = B.a; 

 float norm = 0;
 int i;
 int j;
 for (i = 0; i < m; i++)
 for (j = 0; j < n; j++)
 {
  norm = norm + a[i][j] * b[i][j];	 
 }
 return sqrt(norm);  
}


