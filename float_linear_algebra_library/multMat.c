#include "linear_algebra.h"

void multMat(matrix C, matrix A, matrix B)
{

 if((A.n != B.m) || (A.m != C.m) || (B.n != C.n))
 {
  printf("Matrix dimensions not matching for multiplication. Exiting program. \n");
  exit (1);
 }

 unsigned int m = A.m;
 unsigned int p = A.n;
 unsigned int n = B.n;
 
 float** a = A.a; 
 float** b = B.a;
 float** c = C.a;
 
 int i;
 int j;
 int k;
 for (i = 0; i < m; i++)
 for (j = 0; j < n; j++)
 {
  float sum = 0;
  for (k = 0; k < p; k++)
  {
   sum = sum + a[i][k] * b[k][j];
  }
  c[i][j] = sum;	 
 }  
}


