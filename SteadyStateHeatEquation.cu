#include<stdio.h>
#include<cuda_runtime.h>
#include "cublas_v2.h"
#define nx 2000
#define ny 2000
#define N nx*ny
#define hx 1.0/(nx-1)
#define hy 1.0/(ny-1)
#define g_true 1.0*(nx-1)*(nx-1)
#define f_true 1.0*(ny-1)*(ny-1)
#define d_true -2*(f_true+g_true)
#define gexp -g_true
#define fexp -f_true
#define dexp -d_true
#define dt 0.25
#define gimp -dt*g_true
#define fimp -dt*f_true
#define dimp 1-dt*d_true
#define blocksizex 16
#define blocksizey 16
#define tolerance 0.001

__global__ void initTvector(float *t, float x0value, float y0value, float xmaxvalue, float ymaxvalue);
__global__ void initTvector_ie(float *t, float x0value, float y0value, float xmaxvalue, float ymaxvalue);
__global__ void initQvector(float *q, float pi);
__global__ void initQvector_ie(float *q, float pi);
__global__ void initothervectors(float *t, float *q, float *r, float *p, float *ap);
__global__ void initothervectors_ie(float *t, float *q, float *r, float *p, float *ap);
__global__ void cgkernel(float *p, float *ap);
__global__ void cgkernel_sharedmem(float *p, float *ap);
__global__ void cgkernel_sharedmem2(float *p, float *ap);
__global__ void cgkernel_ie(float *p, float *ap);
__global__ void updateTandR(float *t, float *r, float *p, float *ap, float alpha);
__global__ void updateP(float *p, float *r, float beta);
__global__ void generateanalyticalsolution(float *s, float pi);
__global__ void calculateerror(float *e, float *s, float *t);
float cgsolver();
void cgsolver_ie();

int main()
{
 printf("Solved with error: %f\n", cgsolver());
 return 0;
}

float cgsolver()
{
 float pi = acos(-1);

 // allocate arrays on GPU
 float *t = NULL;
 float *q = NULL;
 float *r = NULL;
 float *p = NULL;
 float *ap = NULL;
 
 // allocate memory for the arrays
 cudaMalloc(&t, N*sizeof(float));
 cudaMalloc(&q, N*sizeof(float));
 cudaMalloc(&r, N*sizeof(float));
 cudaMalloc(&p, N*sizeof(float));
 cudaMalloc(&ap, N*sizeof(float));
 
 // calculate block and grid size
 dim3 block(blocksizex, blocksizey);
 dim3 grid((nx+block.x-1)/block.x,(ny+block.y-1)/block.y);
 
 dim3 block2(blocksizex+2, blocksizey+2); 

 // initialisation of all vectors and other parameters
 initTvector<<<grid,block>>>(t,0,0,0,0);
 initQvector<<<grid,block>>>(q,pi);
 //cudaDeviceSynchronize();
 initothervectors<<<grid,block>>>(t,q,r,p,ap);
 //cudaDeviceSynchronize();
 float r0, ptap, alpha, r1, beta, alphaneg;
 int iter = 0;
 
 // cg method begins
 cublasHandle_t handle;
 cublasStatus_t stat;
 stat = cublasCreate(&handle);
 stat = cublasSnrm2(handle, N, r, 1, &r0);
 while(r0 > tolerance)// && (iter < 100000))
 {
  //printf("while loop entered./n"); 
  cgkernel<<<grid,block>>>(p,ap);
  //cgkernel_sharedmem2<<<grid,block2>>>(p,ap);
  //cudaDeviceSynchronize();
  stat = cublasSdot(handle, N, p, 1, ap, 1, &ptap);
  alpha = (r0*r0)/ptap;
  alphaneg = -alpha;
  stat = cublasSaxpy(handle, N, &alpha, p, 1, t, 1);
  stat = cublasSaxpy(handle, N, &alphaneg, ap, 1, r, 1);
  //updateTandR<<<grid,block>>>(t,r,p,ap,alpha);
  //cudaDeviceSynchronize();
  iter++;
  stat = cublasSnrm2(handle, N, r, 1, &r1);
  if(r1 < tolerance) break;
  beta = (r1*r1)/(r0*r0);
  updateP<<<grid,block>>>(p,r,beta);
  //cudaDeviceSynchronize(); 
  r0 = r1;    
  printf("CG Solver iteration:%i, Residual:%f\n", iter, r1); 
 }  
 printf("CG Solver iterations:%i, Residual:%f\n", iter, r1);
 
 // calculation of analytical solution
 float error;
 float *s = NULL;
 cudaMalloc(&s, N*sizeof(float));
 generateanalyticalsolution<<<grid,block>>>(s,pi);
 float *e = NULL;
 cudaMalloc(&e, N*sizeof(float));
 calculateerror<<<grid,block>>>(e,s,t);
 cudaDeviceSynchronize();
 stat = cublasSnrm2(handle, N, e, 1, &error);
 
 cublasDestroy(handle);
 cudaFree(e);
 cudaFree(s);
 cudaFree(t);
 cudaFree(q);
 cudaFree(r);
 cudaFree(p);
 cudaFree(ap);
 
 return error;
}

__global__ 
void initTvector(float *t, float x0value, float y0value, float xmaxvalue, float ymaxvalue)
{
 int i = threadIdx.x + blocksizex * blockIdx.x;
 int j = threadIdx.y + blocksizey * blockIdx.y;     
 if(i < nx && j < ny)
 {
  int index = i+nx*j;
  t[index] = 1.0;
  if(i == 0) t[index] = x0value;
  if(i == nx-1) t[index] = xmaxvalue;
  if(j == 0) t[index] = ymaxvalue;
  if(j == ny-1) t[index] = y0value;  
 }  
}

__global__ 
void initQvector(float *q, float pi)
{
 int i = threadIdx.x + blocksizex * blockIdx.x;
 int j = threadIdx.y + blocksizey * blockIdx.y;     
 if(i < nx && j < ny)
 {
  int index = i+nx*j;
  q[index] = -2*pi*pi*sinf(pi*i*hx)*sinf(pi*(ny-1-j)*hy); // x=i*hx, y=(ny-1-j)*hy   
 }
}

__global__ 
void initothervectors(float *t, float *q, float *r, float *p, float *ap)
{
 int i = threadIdx.x + blocksizex * blockIdx.x;
 int j = threadIdx.y + blocksizey * blockIdx.y;     
 if(i < nx && j < ny)
 {
  int index = i+nx*j;
  float temp = 0;
  if(i > 0 && i < nx-1 && j > 0 && j < ny-1)
  {
   temp = -q[index]-gexp*(t[index-1]+t[index+1])-fexp*(t[index-nx]+t[index+nx])-dexp*t[index]; 
  }
  r[index] = temp;
  p[index] = temp;
  ap[index] = temp;     
 }
}

__global__ 
void initothervectors_ie(float *t, float *q, float *r, float *p, float *ap)
{
 int i = threadIdx.x + blocksizex * blockIdx.x;
 int j = threadIdx.y + blocksizey * blockIdx.y;     
 if(i < nx && j < ny)
 {
  int index = i+nx*j;
  float temp = 0;
  if(i > 0 && i < nx-1 && j > 0 && j < ny-1)
  {
   temp = q[index]-gimp*(t[index-1]+t[index+1])-fimp*(t[index-nx]+t[index+nx])-dimp*t[index];    
  }
  r[index] = temp;
  p[index] = temp;
  ap[index] = temp;     
 }
}

__global__ 
void cgkernel(float *p, float *ap)
{
 int i = threadIdx.x + blockDim.x * blockIdx.x;
 int j = threadIdx.y + blockDim.y * blockIdx.y;     
 if(i > 0 && i < nx-1 && j > 0 && j < ny-1)
 {
  int index = i+nx*j;
  ap[index] = gexp*(p[index-1]+p[index+1])+fexp*(p[index-nx]+p[index+nx])+dexp*p[index];    
 }
}

__global__ 
void cgkernel_sharedmem(float *p, float *ap)
{
 register int ii = threadIdx.x;
 register int jj = threadIdx.y;	
 register int i = ii + blocksizex * blockIdx.x;
 register int j = jj + blocksizey * blockIdx.y;     
 __shared__ float temp[blocksizey+2][blocksizex+2];
 register int index = i+nx*j;
 temp[jj+1][ii+1] = p[index];
 if(jj == 0) {temp[0][ii+1] = p[index-nx];}
 else if(jj == blocksizey-1) {temp[blocksizey+1][ii+1] = p[index+nx];}
 else if(ii == 0 && j != 0 && jj != blocksizey-1) {temp[jj+1][0] = p[index-1];}
 else if(ii == blocksizex-1 && jj != 0 && jj != blocksizey-1) {temp[jj+1][blocksizex+1] = p[index+1];}
 else if(ii == 1 && jj == 1) {temp[1][0] = p[index-2-nx];}
 else if(ii == blocksizex-2 && jj == 1) {temp[1][blocksizex+1] = p[index+2-nx];}
 else if(ii == 1 && jj == blocksizey-2) {temp[blocksizey][0] = p[index-2+nx];}
 else if(ii == blocksizex-2 && jj == blocksizey-2) {temp[blocksizey][blocksizex+1] = p[index+2+nx];}
 __syncthreads();
  
 if(i > 0 && i < nx-1 && j > 0 && j < ny-1)
  {	  
    ap[index] = gexp*(temp[jj+1][ii]+temp[jj+1][ii+2])+fexp*(temp[jj][ii+1]+temp[jj+2][ii+1])+dexp*temp[jj+1][ii+1];
  }  
}

__global__
void cgkernel_sharedmem2(float *p, float *ap)
{
 __shared__ float temp[blocksizey+2][blocksizex+2];
 int ii = threadIdx.x;
 int jj = threadIdx.y;
 int i = ii-1 + blocksizex * blockIdx.x;
 int j = jj-1 + blocksizey * blockIdx.y;
 
 int index = i+nx*j;
 temp[jj][ii] = p[index];
 __syncthreads();

 if(i > 0 && i < nx-1 && j > 0 && j < ny-1)
  {
   if(ii > 0 && ii < blocksizex+1 && jj > 0 && jj < blocksizey+1)
   {
    ap[index] = gexp*(temp[jj][ii-1]+temp[jj][ii+1])+fexp*(temp[jj+1][ii]+temp[jj-1][ii])+dexp*temp[jj][ii];
   }
  }
}


__global__ 
void cgkernel_ie(float *p, float *ap)
{
 int i = threadIdx.x + blocksizex * blockIdx.x;
 int j = threadIdx.y + blocksizey * blockIdx.y;     
 if(i > 0 && i < nx-1 && j > 0 && j < ny-1)
 {
  int index = i+nx*j;
  ap[index] = gimp*(p[index-1]+p[index+1])+fimp*(p[index-nx]+p[index+nx])+dimp*p[index];    
 }
}

__global__ 
void updateTandR(float *t, float *r, float *p, float *ap, float alpha)
{
 int i = threadIdx.x + blocksizex * blockIdx.x;
 int j = threadIdx.y + blocksizey * blockIdx.y;     
 if(i > 0 && i < nx-1 && j > 0 && j < ny-1)
 {
  int index = i+nx*j;
  t[index] += alpha*p[index];
  r[index] -= alpha*ap[index];    
 }
}

__global__ 
void updateP(float *p, float *r, float beta)
{
 int i = threadIdx.x + blocksizex * blockIdx.x;
 int j = threadIdx.y + blocksizey * blockIdx.y;     
 if(i > 0 && i < nx-1 && j > 0 && j < ny-1)
 {
  int index = i+nx*j;
  p[index] = r[index] + beta*p[index];    
 }
}

__global__ 
void generateanalyticalsolution(float *s, float pi)
{
 int i = threadIdx.x + blocksizex * blockIdx.x;
 int j = threadIdx.y + blocksizey * blockIdx.y;     
 if(i < nx && j < ny)
 {
  int index = i+nx*j;
  s[index] = sinf(pi*i*hx)*sinf(pi*(ny-1-j)*hy); // x=i*hx, y=(ny-1-j)*hy   
 } 
}

__global__ 
void calculateerror(float *e, float *s, float *t)
{
 int i = threadIdx.x + blocksizex * blockIdx.x;
 int j = threadIdx.y + blocksizey * blockIdx.y;     
 if(i < nx && j < ny)
 {
  int index = i+nx*j;
  e[index] = s[index]-t[index];   
 } 
}





