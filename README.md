# CUDA based GPU computing to solve heat eqn

Programs for solving heat equation on Nvidia GPU using CUDA. Relevant presentation attached. 

This program uses a custom linear algebra library for performing operations on small dense matrices. 

To create the library, follow the steps:

1. compile the optimized object files with the link-time optimization option: gcc -c -O3 -flto *.c

2. create the library using ar utility: ar -cvq liblinearalgebra.a *.o

Object files present in the library can be checked using: ar -t liblinearalgebra.a

Now compile the C program using:

gcc -flto -fopenmp -O3 -I../Seminar/linear_algebra_library HeatEquation.c -lm linear_algebra_library/liblinearalgebra.a -o Heat.exe

(This is the standard order of compilation flags, the libraries must always come after the program translation unit.)

NOTE:

1. Folder placement might change. So correct path must be provided to include the linear algebra library.

2. Compile Cuda codes using nvcc. These are the codes:

	(i) SteayStateHeatEquation: Homogeneous.

	(ii) DynamicHeatEquation: Time-varying. Solved using backward euler method. Conjugate-gradient method used for solving the eqn.

	(iii) Implicit_Jacobi_GL: Time-varying. Solved using backward euler method, with Jacobi method. OpenGL lib. used for visualization.
